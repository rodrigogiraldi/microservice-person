package com.example.microserviceperson.service

import com.example.microserviceperson.dto.CityDTO
import com.example.microserviceperson.entity.Person
import com.example.microserviceperson.repository.PersonRepository
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForEntity

@Service
class PersonService(val personRepository: PersonRepository) {

    val CITY_SERVICE_URL = "http://localhost:8090/"

    fun listAll() = personRepository.findAll()

    fun add(person: Person) {
        if(cityExists(person.city)){
            personRepository.save(person)
        }
    }

    fun cityExists(cityId: Int): Boolean{
        val restTemplate = RestTemplate()
        val response = restTemplate.getForEntity("${CITY_SERVICE_URL}city/${cityId}", CityDTO().javaClass)

        return response.body != null && response.body!!.id != 0L
    }
}