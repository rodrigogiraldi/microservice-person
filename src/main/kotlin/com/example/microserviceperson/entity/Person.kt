package com.example.microserviceperson.entity

import javax.persistence.*

@Entity
@Table
class Person {
    @Id
    @GeneratedValue
    @Column
    var id: Long = 0

    @Column
    var name: String = ""

    @Column
    var city: Int = 0
}