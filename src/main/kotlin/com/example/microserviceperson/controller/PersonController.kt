package com.example.microserviceperson.controller

import com.example.microserviceperson.entity.Person
import com.example.microserviceperson.service.PersonService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/person")
class PersonController(val personService: PersonService) {

    @GetMapping("/")
    fun listAll() = personService.listAll()

    @PostMapping("/")
    fun add(@RequestBody person: Person) = personService.add(person)
}