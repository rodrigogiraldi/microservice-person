package com.example.microserviceperson.repository

import com.example.microserviceperson.entity.Person
import org.springframework.data.jpa.repository.JpaRepository

interface PersonRepository : JpaRepository<Person, Long> {
}