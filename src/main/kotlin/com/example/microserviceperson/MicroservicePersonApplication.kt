package com.example.microserviceperson

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MicroservicePersonApplication

fun main(args: Array<String>) {
	runApplication<MicroservicePersonApplication>(*args)
}
