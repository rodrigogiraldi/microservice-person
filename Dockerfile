FROM adoptopenjdk/openjdk11:latest
COPY target/microservice-person.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]